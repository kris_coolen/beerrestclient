package be.kriscoolen.beer_restclient.client;

import be.kriscoolen.beer_restclient.domain.*;
import org.springframework.web.util.UriBuilder;

import java.net.URI;

public interface BeerClient {
    //methods from BeerRepository
    public Beer getBeerById(int id);
    public BeerList getBeersByAlcohol(float alcohol);
    public BeerList getBeersContainingName(String partOfName);

    //methods from beerService and OrderRepository
    public OrderList getBeerOrders();
    public URI makeBeerOrder(Order order);


}
