package be.kriscoolen.beer_restclient.client;

import be.kriscoolen.beer_restclient.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class BeerClientImpl implements BeerClient {
    private String baseUrl;
    private RestTemplate template;

    @Value("${baseUrl}")
    public void setBaseUrl(String baseUrl){
        this.baseUrl=baseUrl;
    }

    @Autowired
    public void setTemplate(RestTemplate template){
        this.template=template;
    }

    @Override
    public Beer getBeerById(int id) {
        try
        {
            ResponseEntity<Beer> response = template.getForEntity(baseUrl + "/beers/{id}", Beer.class, id);
            if(response.getStatusCode()== HttpStatus.OK){
                return response.getBody();
            }
        }catch(Exception e){
        }
        return null;
    }

    @Override
    public BeerList getBeersByAlcohol(float alcohol) {
        ResponseEntity<BeerList> response = template.getForEntity
                (baseUrl+"/beers?alcohol={0}",BeerList.class,alcohol);
        if(response.getStatusCode()==HttpStatus.OK){
            return response.getBody();
        }
        else return null;
    }

    @Override
    public BeerList getBeersContainingName(String partOfName) {
        ResponseEntity<BeerList> response = template.getForEntity
                (baseUrl+"/beers?name={0}",BeerList.class,partOfName);
        if(response.getStatusCode()==HttpStatus.OK){
            return response.getBody();
        }
        else return null;
    }

    @Override
    public OrderList getBeerOrders() {
        ResponseEntity<OrderList> response = template.getForEntity
                (baseUrl+"/orders",OrderList.class);
        if(response.getStatusCode()==HttpStatus.OK){
            return response.getBody();
        }
        else return null;
    }

    @Override
    public URI makeBeerOrder(Order order) {
        return template.postForLocation(baseUrl+"/orders",order);
    }
}
