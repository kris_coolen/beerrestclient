package be.kriscoolen.beer_restclient.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name="OrderList")
public class OrderList {

    private List<BeerOrder> orderList;

    public OrderList(){}

    public OrderList(List<BeerOrder> orderList) {
        this.orderList = orderList;
    }

    @JsonProperty("BeerOrders")
    @XmlElementWrapper(name="BeerOrders")
    @XmlElement(name="Beer")
    public List<BeerOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<BeerOrder> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        return orderList.toString();
    }
}
