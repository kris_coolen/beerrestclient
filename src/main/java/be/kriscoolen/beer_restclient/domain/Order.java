package be.kriscoolen.beer_restclient.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
public class Order implements Serializable {

    private String orderName;
    private List<Integer> beerIdList;
    private List<Integer> quantityList;

    public Order(){}

    public Order(String orderName, List<Integer> beerIdList, List<Integer> quantityList) {
        this.orderName = orderName;
        this.beerIdList = beerIdList;
        this.quantityList = quantityList;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public List<Integer> getBeerIdList() {
        return beerIdList;
    }

    public void setBeerIdList(List<Integer> beerIdList) {
        this.beerIdList = beerIdList;
    }

    public List<Integer> getQuantityList() {
        return quantityList;
    }

    public void setQuantityList(List<Integer> quantityList) {
        this.quantityList = quantityList;
    }
}
