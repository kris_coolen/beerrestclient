package be.kriscoolen.beer_restclient.domain;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="BeerOrderItems")
@XmlRootElement(name="Item")
public class BeerOrderItem {
   @Id
   @GeneratedValue(strategy=GenerationType.IDENTITY)
   @Column(name="Id")
   private int id;


   @ManyToOne
   @JoinColumn(name="BeerId")
   private Beer beer;
   
   @Column(name="Number")
   private int number;

   public BeerOrderItem() {
   }

   public BeerOrderItem(Beer beer, int number){
      this.beer = beer;
      this.number = number;
   }

   public Beer getBeer() {
      return beer;
   }

   public void setBeer(Beer beer) {
      this.beer = beer;
   }

   public int getNumber() {
      return number;
   }

   public void setNumber(int number) {
      this.number = number;
   }

   public int getId() {
      return id;
   }

   @Override
   public String toString() {
      return "BeerOrderItem{" +
              "beer=" + beer +
              ", number=" + number +
              '}';
   }
}
