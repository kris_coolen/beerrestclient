package be.kriscoolen.beer_restclient.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name="BeerList")
public class BeerList implements Serializable {
    private List<Beer> beerList;

    public BeerList(){}

    public BeerList(List<Beer> beerList) {
        this.beerList = beerList;
    }

    @JsonProperty("Beers")
    @XmlElementWrapper(name="Beers")
    @XmlElement(name="Beer")
    public List<Beer> getBeerList() {
        return beerList;
    }

    public void setBeerList(List<Beer> beerList) {
        this.beerList = beerList;
    }

    @Override
    public String toString() {
        return beerList.toString();
    }
}
