package be.kriscoolen.beer_restclient;

import be.kriscoolen.beer_restclient.client.BeerClient;
import be.kriscoolen.beer_restclient.domain.Order;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class BeersClientApp {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.requestFactory(HttpComponentsClientHttpRequestFactory::new)
                .basicAuthentication("homer","password")
                .build();
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(BeersClientApp.class,args);
        BeerClient beerClient = ctx.getBean(BeerClient.class);
        System.out.println("Beer with id 4: " + beerClient.getBeerById(4));
        System.out.println("Beer with id 1: " + beerClient.getBeerById(1));
        System.out.println("Beers with alcohol 0.0:"+ beerClient.getBeersByAlcohol(0.0f));
        System.out.println("Beers with alcohol 5.5:"+ beerClient.getBeersByAlcohol(5.5f));
        System.out.println("Beers containing name 'chimay':"
        +beerClient.getBeersContainingName("chimay"));

        System.out.println("overview of all orders:");
        System.out.println(beerClient.getBeerOrders());
        //create a new order
        List<Integer> beerIdList = new ArrayList<>();
        List<Integer> quantityList = new ArrayList<>();
        beerIdList.add(127);
        beerIdList.add(128);
        quantityList.add(1);
        quantityList.add(5);
        Order order = new Order("client-test-order",beerIdList,quantityList);
        System.out.println("make a new beerorder:");
        System.out.println("URI: " + beerClient.makeBeerOrder(order));


    }
}
